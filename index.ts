import App from "./app";
const PORT = process.env.PORT || 3000;

App.listen(PORT, () => {
    console.log(`API REST corriendo en puerto ${PORT}`);
})